library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity template_regs is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        hello0: out std_logic_vector (3 downto 0);
        world0: out std_logic_vector (3 downto 0);

        hello1: out std_logic_vector (3 downto 0);
        world1: out std_logic_vector (3 downto 0);

        iWr: in std_logic;
        iRd: in std_logic;
        iAddr: in std_logic_vector (0 downto 0);
        iData: in std_logic_vector (7 downto 0);
        oNd: out std_logic;
        oData: out std_logic_vector (7 downto 0)
    );
end entity;

architecture v1 of template_regs is

    signal sData: std_logic_vector (iData'range);

    signal sNd0: std_logic;
    signal sNd1: std_logic;
--------------------------------------------------------------------------------
-- signals related to hello_reg

    constant chello_reg_addr: integer := 0;
    signal hello_reg_wr: std_logic;
    signal hello0_reg: std_logic_vector (3 downto 0);
    signal world0_reg: std_logic_vector (3 downto 0);
    signal hello0_rd: std_logic_vector (3 downto 0);
    signal world0_rd: std_logic_vector (3 downto 0);

--------------------------------------------------------------------------------
-- signals related to world_reg

    constant cworld_reg_addr: integer := 1;
    signal world_reg_wr: std_logic;
    signal hello1_reg: std_logic_vector (3 downto 0);
    signal world1_reg: std_logic_vector (3 downto 0);
    signal hello1_rd: std_logic_vector (3 downto 0);
    signal world1_rd: std_logic_vector (3 downto 0);

begin

    oNd <= sNd1;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sNd0 <= '0';
                sNd1 <= '0';
            else
                sNd0 <= iRd;
                sNd1 <= sNd0;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sData <= (others => '0');
            else
                sData <= iData;
            end if;
        end if;
    end process;

--------------------------------------------------------------------------------
-- signals related to hello_reg

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                hello_reg_wr <= '0';
            else
                if iAddr = std_logic_vector(to_unsigned(chello_reg_addr,
                    iAddr'length)) then
                    hello_reg_wr <= iWr;
                else
                    hello_reg_wr <= '0';
                end if;
            end if;
        end if;
    end process;

    hello0 <= hello0_reg;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                hello0_reg <= (others => '0');
            else
                if hello_reg_wr = '1' then
                    hello0_reg <= sData (3 downto 0);
                end if;
            end if;
        end if;
    end process;

    world0 <= world0_reg;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                world0_reg <= (others => '0');
            else
                if hello_reg_wr = '1' then
                    world0_reg <= sData (7 downto 4);
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                hello0_rd <= (others => '0');
            else
                if iAddr = std_logic_vector(to_unsigned(chello_reg_addr,
                    iAddr'length)) and iRd = '1' then
                    hello0_rd <= hello0_reg;
                else
                    hello0_rd <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                world0_rd <= (others => '0');
            else
                if iAddr = std_logic_vector(to_unsigned(chello_reg_addr,
                    iAddr'length)) and iRd = '1' then
                    world0_rd <= world0_reg;
                else
                    world0_rd <= (others => '0');
                end if;
            end if;
        end if;
    end process;

--------------------------------------------------------------------------------
-- signals related to world_reg

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                world_reg_wr <= '0';
            else
                if iAddr = std_logic_vector(to_unsigned(cworld_reg_addr,
                    iAddr'length)) then
                    world_reg_wr <= iWr;
                else
                    world_reg_wr <= '0';
                end if;
            end if;
        end if;
    end process;

    hello1 <= hello1_reg;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                hello1_reg <= (others => '0');
            else
                if world_reg_wr = '1' then
                    hello1_reg <= sData (3 downto 0);
                end if;
            end if;
        end if;
    end process;

    world1 <= world1_reg;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                world1_reg <= (others => '0');
            else
                if world_reg_wr = '1' then
                    world1_reg <= sData (7 downto 4);
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                hello1_rd <= (others => '0');
            else
                if iAddr = std_logic_vector(to_unsigned(cworld_reg_addr,
                    iAddr'length)) and iRd = '1' then
                    hello1_rd <= hello1_reg;
                else
                    hello1_rd <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                world1_rd <= (others => '0');
            else
                if iAddr = std_logic_vector(to_unsigned(cworld_reg_addr,
                    iAddr'length)) and iRd = '1' then
                    world1_rd <= world1_reg;
                else
                    world1_rd <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                oData <= (others => '0');
            else
                if sNd0 = '1' then
                    oData (0) <= hello0_rd (0) or hello1_rd (0);
                    oData (1) <= hello0_rd (1) or hello1_rd (1);
                    oData (2) <= hello0_rd (2) or hello1_rd (2);
                    oData (3) <= hello0_rd (3) or hello1_rd (3);
                    oData (4) <= world0_rd (0) or world1_rd (0);
                    oData (5) <= world0_rd (1) or world1_rd (1);
                    oData (6) <= world0_rd (2) or world1_rd (2);
                    oData (7) <= world0_rd (3) or world1_rd (3);
                end if;
            end if;
        end if;
    end process;

end v1;

