{-# LANGUAGE DeriveGeneric #-}

module HdlRegsGenerator (
    writeTop,
    RegConfig (..),
    Reg (..),
    RegSlice (..)
) where

import GHC.Generics
import Data.List
import Data.List.Split

data RegSlice = RegSlice {
    sliceName :: String,
    mode :: String,
    lowAddress :: Int,
    highAddress :: Int
} deriving (Show, Generic)

data Reg = Reg {
    regName :: String,
    address :: Int,
    slices :: [RegSlice]
} deriving (Show, Generic)

data RegConfig = RegConfig {
    name :: String,
    regs :: [Reg]
} deriving (Show, Generic)

writeTop :: RegConfig -> String
writeTop a = let
        libs = writeLibs
        entity = writeTopEntity a
        architecture = writeTopArch a
    in
        refineSources (libs ++ entity ++ architecture)

writeLibs :: String
writeLibs =
    "library ieee;\n" ++
    "use ieee.std_logic_1164.all;\n" ++
    "use ieee.numeric_std.all;\n\n\n"


writeTopEntity :: RegConfig -> String
writeTopEntity a = let
        e00 = "entity "
        e01 = name a
        e02 = " is\n"
        e03 = "    port (\n"
        e04 = "        iClk: in std_logic;\n"
        e05 = "        iReset: in std_logic;\n\n"
        e06 = writeOutPorts (regs a) "        " ++ "\n"
        e07 = "        iWr: in std_logic;\n"
        e08 = "        iRd: in std_logic;\n"
        e09 = "        iAddr: in std_logic_vector (" ++
            show (addrWidth (regs a)-1) ++ " downto 0);\n"
        e10 = "        iData: in std_logic_vector (" ++
            show (highIndex (regs a)) ++ " downto 0);\n"
        e11 = "        oNd: out std_logic;\n"
        e12 = "        oData: out std_logic_vector (" ++
            show (highIndex (regs a)) ++ " downto 0)\n"
        e13 = "    );\n"
        e14 = "end entity;\n\n"
    in
        e00 ++ e01 ++ e02 ++ e03 ++ e04 ++ e05 ++ e06 ++ e07 ++ e08 ++ e09 ++
            e10 ++ e11 ++ e12 ++ e13 ++ e14

writeOutPorts :: [Reg] -> String -> String
writeOutPorts [] _ = ""
writeOutPorts [a] offset = writeOutPortsOne a offset
writeOutPorts (a:b) offset = writeOutPortsOne a offset ++ "\n" ++
    writeOutPorts b offset

writeOutPortsOne :: Reg -> String -> String
writeOutPortsOne a offset = writeOutPortSlices (slices a) offset

writeOutPortSlices :: [RegSlice] -> String -> String
writeOutPortSlices [] _ = ""
writeOutPortSlices [a] offset = writeOutPortSliceOne a offset
writeOutPortSlices (a:b) offset = writeOutPortSliceOne a offset ++
    writeOutPortSlices b offset

writeOutPortSliceOne :: RegSlice -> String -> String
writeOutPortSliceOne a offset = offset ++ sliceName a ++ ": out " ++
    writeSliceType a ++ ";\n"

writeSliceType :: RegSlice -> String
writeSliceType a
    | lowAddress a == highAddress a = "std_logic"
    | otherwise = "std_logic_vector (" ++
        show ((highAddress a) - (lowAddress a)) ++ " downto 0)"

writeTopArch :: RegConfig -> String
writeTopArch a = let
        a00 = "architecture v1 of " ++ name a ++ " is\n\n"
        a01 = "    signal sData: std_logic_vector (iData'range);\n\n"
        a02 = "    signal sNd0: std_logic;\n"
        a03 = "    signal sNd1: std_logic;\n"
        a04 = makeRegSignals (regs a)
        a05 = "begin\n\n"
        a06 = "    oNd <= sNd1;\n\n"
        a07 = "    process (iClk)\n"
        a08 = "    begin\n"
        a09 = "        if iClk'event and iClk = '1' then\n"
        a10 = "            if iReset = '1' then\n"
        a11 = "                sNd0 <= '0';\n"
        a12 = "                sNd1 <= '0';\n"
        a13 = "            else\n"
        a14 = "                sNd0 <= iRd;\n"
        a15 = "                sNd1 <= sNd0;\n"
        a16 = "            end if;\n"
        a17 = "        end if;\n"
        a18 = "    end process;\n\n"
        a19 = "    process (iClk)\n"
        a20 = "    begin\n"
        a21 = "        if iClk'event and iClk = '1' then\n"
        a22 = "            if iReset = '1' then\n"
        a23 = "                sData <= (others => '0');\n"
        a24 = "            else\n"
        a25 = "                sData <= iData;\n"
        a26 = "            end if;\n"
        a27 = "        end if;\n"
        a28 = "    end process;\n\n"
        a29 = makeArchBodyRegAr (regs a)
        a30 = "    process (iClk)\n"
        a31 = "    begin\n"
        a32 = "        if iClk'event and iClk = '1' then\n"
        a33 = "            if iReset = '1' then\n"
        a34 = "                oData <= (others => '0');\n"
        a35 = "            else\n"
        a36 = "                if sNd0 = '1' then\n"
        a37 = readDataAssignments (regs a)
        a38 = "                end if;\n"
        a39 = "            end if;\n"
        a40 = "        end if;\n"
        a41 = "    end process;\n\n"
        a42 = "end v1;\n"
    in
        a00 ++ a01 ++ a02 ++ a03 ++ a04 ++ a05 ++ a06 ++ a07 ++ a08 ++ a09 ++
            a10 ++ a11 ++ a12 ++ a13 ++ a14 ++ a15 ++ a16 ++ a17 ++ a18 ++
            a19 ++ a20 ++ a21 ++ a22 ++ a23 ++ a24 ++ a25 ++ a26 ++ a27 ++
            a28 ++ a29 ++ a30 ++ a31 ++ a32 ++ a33 ++ a34 ++ a35 ++ a36 ++
            a37 ++ a38 ++ a39 ++ a40 ++ a41 ++ a42

makeRegSignals :: [Reg] -> String
makeRegSignals [] = ""
makeRegSignals [a] = makeRegSignalsOne a
makeRegSignals (a:b) = makeRegSignalsOne a ++ makeRegSignals b

makeRegSignalsOne :: Reg -> String
makeRegSignalsOne a = let
        s0 = "----------------------------------------" ++
            "----------------------------------------\n"
        s1 = "-- signals related to " ++ regName a ++ "\n\n"
        s2 = "    constant c" ++ regName a ++ "_addr: integer := " ++
            show (address a) ++ ";\n"
        s3 = "    signal " ++ regName a ++ "_wr" ++ ": std_logic;\n"
        s4 = makeRegSignalsOneSlices (slices a) "_reg"
        s5 = makeRegSignalsOneSlices (slices a) "_rd"
    in
        s0 ++ s1 ++ s2 ++ s3 ++ s4 ++ s5 ++ "\n"

makeRegSignalsOneSlices :: [RegSlice] -> String -> String
makeRegSignalsOneSlices [] c = ""
makeRegSignalsOneSlices [a] c = makeRegSignalsOneSlicesWorker a c
makeRegSignalsOneSlices (a:b) c = makeRegSignalsOneSlicesWorker a c ++
    makeRegSignalsOneSlices b c

makeRegSignalsOneSlicesWorker :: RegSlice -> String -> String
makeRegSignalsOneSlicesWorker a b = "    signal " ++ sliceName a ++ b ++ ": " ++
    writeSliceType a ++ ";\n"

makeArchBodyRegAr :: [Reg] -> String
makeArchBodyRegAr [] = ""
makeArchBodyRegAr [a] = makeArchBodyReg a
makeArchBodyRegAr (a:b) = makeArchBodyReg a ++ makeArchBodyRegAr b

makeArchBodyReg :: Reg -> String
makeArchBodyReg a = let
        wrName = regName a ++ "_wr"
        rdName = regName a ++ "_rd"
        addrName = regName a ++ "_addr"
        oWireName = regName a ++ "_owire"
        highIndex = getHighIndex (slices a)
        s0 = "----------------------------------------" ++
            "----------------------------------------\n"
        s1 = "-- signals related to " ++ regName a ++ "\n\n"
        p0l00 = "    process (iClk)\n"
        p0l01 = "    begin\n"
        p0l02 = "        if iClk'event and iClk = '1' then\n"
        p0l03 = "            if iReset = '1' then\n"
        p0l04 = "                " ++ wrName ++ " <= '0';\n"
        p0l05 = "            else\n"
        p0l06 = "                if iAddr = std_logic_vector(to_unsigned(c" ++
            addrName ++ ", iAddr'length)) then\n"
        p0l07 = "                    " ++ wrName ++ " <= iWr;\n"
        p0l08 = "                else\n"
        p0l09 = "                    " ++ wrName ++ " <= '0';\n"
        p0l10 = "                end if;\n"
        p0l11 = "            end if;\n"
        p0l12 = "        end if;\n"
        p0l13 = "    end process;\n\n"

        p1l00 = makeArchBodyAssignSliceAr (slices a) wrName
        p1l01 = rdAssignments (slices a) addrName

    in
        s0 ++ s1 ++ p0l00 ++ p0l01 ++ p0l02 ++ p0l03 ++ p0l04 ++ p0l05 ++
            p0l06 ++ p0l07 ++ p0l08 ++ p0l09 ++ p0l10 ++ p0l11 ++ p0l12 ++
            p0l13 ++ p1l00 ++ p1l01

rdAssignments :: [RegSlice] -> String -> String
rdAssignments [] c = ""
rdAssignments [a] c = rdAssignmentsWorker a c
rdAssignments (a:b) c = rdAssignmentsWorker a c ++ rdAssignments b c

rdAssignmentsWorker :: RegSlice -> String -> String
rdAssignmentsWorker a b = let
        name = sliceName a ++ "_reg"
        nameT = sliceName a ++ "_rd"
        range = highAddress a - lowAddress a
        defVal = sliceDefault range
        p4l00 = "    process (iClk)\n"
        p4l01 = "    begin\n"
        p4l02 = "        if iClk'event and iClk = '1' then\n"
        p4l03 = "            if iReset = '1' then\n"
        p4l04 = "                " ++ nameT ++ " <= " ++ defVal ++ ";\n"
        p4l05 = "            else\n"
        p4l06 = "                if iAddr = std_logic_vector(to_unsigned(c" ++
            b ++ ", iAddr'length)) and iRd = '1' then\n"
        p4l07 = "                    " ++ nameT ++ " <= " ++ name ++ ";\n"
        p4l08 = "                else\n"
        p4l09 = "                    " ++ nameT ++ " <= " ++ defVal ++ ";\n"
        p4l10 = "                end if;\n"
        p4l11 = "            end if;\n"
        p4l12 = "        end if;\n"
        p4l13 = "    end process;\n\n"
    in
        p4l00 ++ p4l01 ++ p4l02 ++ p4l03 ++ p4l04 ++ p4l05 ++ p4l06 ++ p4l07 ++
        p4l08 ++ p4l09 ++ p4l10 ++ p4l11 ++ p4l12 ++ p4l13

makeArchBodyAssignSliceAr :: [RegSlice] -> String -> String
makeArchBodyAssignSliceAr [] c = ""
makeArchBodyAssignSliceAr [a] c = makeArchBodyAssignSlices a c
makeArchBodyAssignSliceAr (a:b) c = makeArchBodyAssignSlices a c ++
    makeArchBodyAssignSliceAr b c

makeArchBodyAssignSlices :: RegSlice -> String -> String
makeArchBodyAssignSlices a b = let
        s00 = "    " ++ sliceName a ++ " <= " ++ sliceName a ++ "_reg;\n\n"
        s01 = "    process (iClk)\n"
        s02 = "    begin\n"
        s03 = "        if iClk'event and iClk = '1' then\n"
        s04 = "            if iReset = '1' then\n"
        s05 = "                " ++ sliceName a ++ "_reg <= " ++
            sliceDefault (highAddress a - lowAddress a) ++ ";\n"
        s06 = "            else\n"
        s07 = "                if " ++ b ++ " = '1' then\n"
        s08 = "                    " ++ sliceName a ++ "_reg <= sData (" ++
            show (highAddress a) ++ " downto " ++ show (lowAddress a) ++ ");\n"
        s09 = "                end if;\n"
        s10 = "            end if;\n"
        s11 = "        end if;\n"
        s12 = "    end process;\n\n"
    in
        s00 ++ s01 ++ s02 ++ s03 ++ s04 ++ s05 ++ s06 ++ s07 ++ s08 ++ s09 ++
            s10 ++ s11 ++ s12

sliceDefault :: Int -> String
sliceDefault a
    | a == 1 = "'0'"
    | otherwise = "(others => '0')"

getHighIndex :: [RegSlice] -> Int
getHighIndex a = getHighIndexWorker a 0

getHighIndexWorker :: [RegSlice] -> Int -> Int
getHighIndexWorker [] c = c
getHighIndexWorker [a] c = getHighIndexWorkerCmp a c
getHighIndexWorker (a:b) c = getHighIndexWorker b (getHighIndexWorkerCmp a c)

getHighIndexWorkerCmp :: RegSlice -> Int -> Int
getHighIndexWorkerCmp a b
    | highAddress a > b = highAddress a
    | otherwise = b

readDataAssignments :: [Reg] -> String
readDataAssignments a = readDataAssignmentsLow (makeRegList a)

makeRegList :: [Reg] -> [RegSlice]
makeRegList [] = []
makeRegList [a] = slices a
makeRegList (a:b) = slices a ++ makeRegList b

readDataAssignmentsLow :: [RegSlice] -> String
readDataAssignmentsLow a = readDataAssignmentsLowWorker a 0

readDataAssignmentsLowWorker :: [RegSlice] -> Int -> String
readDataAssignmentsLowWorker [] index = ""
readDataAssignmentsLowWorker a index = readDataAssignmentsLowWorkerLoop a index

readDataAssignmentsLowWorkerLoop :: [RegSlice] -> Int -> String
readDataAssignmentsLowWorkerLoop a index = let
        t = readDataAssignmentsLowWorkerFilter a index False
        c = readDataAssignmentsLowWorkerFilter a index True
        s = readDataAssignmentsLowWorkerLine c index True
        h = stringGate t (anotherSmallFunction c index ++ s ++ ";\n")
    in
        h ++ readDataAssignmentsLowWorker t (index+1)

anotherSmallFunction :: [RegSlice] -> Int -> String
anotherSmallFunction [] index =
    "                    oData (" ++ show index ++ ") <= '0'"
anotherSmallFunction _ index =
    "                    oData (" ++ show index ++ ") <= "

stringGate :: [a] -> String -> String
stringGate [] s = ""
stringGate _ s = s

readDataAssignmentsLowWorkerFilter :: [RegSlice] -> Int -> Bool -> [RegSlice]
readDataAssignmentsLowWorkerFilter [] _  _ = []
readDataAssignmentsLowWorkerFilter [a] index remove =
    readDataAssignmentsLowWorkerFilterW a index remove
readDataAssignmentsLowWorkerFilter (a:b) index remove =
    readDataAssignmentsLowWorkerFilterW a index remove ++
    readDataAssignmentsLowWorkerFilter b index remove

readDataAssignmentsLowWorkerFilterW :: RegSlice -> Int -> Bool -> [RegSlice]
readDataAssignmentsLowWorkerFilterW a index True
    | readDataAssignmentsLowWorkerFilterWC a index = [a]
    | otherwise = []
readDataAssignmentsLowWorkerFilterW a index False
    | readDataAssignmentsLowWorkerFilterWC2 a index = []
    | otherwise = [a]

readDataAssignmentsLowWorkerFilterWC :: RegSlice -> Int -> Bool
readDataAssignmentsLowWorkerFilterWC a index
    | index >= lowAddress a && index <= highAddress a = True
    | otherwise = False

readDataAssignmentsLowWorkerFilterWC2 :: RegSlice -> Int -> Bool
readDataAssignmentsLowWorkerFilterWC2 a index
    | index > highAddress a = True
    | otherwise = False

readDataAssignmentsLowWorkerLine :: [RegSlice] -> Int -> Bool -> String
readDataAssignmentsLowWorkerLine [] _ _ = ""
readDataAssignmentsLowWorkerLine [a] index first =
    readDataAssignmentsLowWorkerLineW a index first
readDataAssignmentsLowWorkerLine (a:b) index first =
    readDataAssignmentsLowWorkerLineW a index first ++
    readDataAssignmentsLowWorkerLine b index False

readDataAssignmentsLowWorkerLineW :: RegSlice -> Int -> Bool -> String
readDataAssignmentsLowWorkerLineW a index first
    | readDataAssignmentsLowWorkerFilterWC a index =
        readDataAssignmentsLowWorkerLineWC a index first
    | otherwise = ""

readDataAssignmentsLowWorkerLineWC :: RegSlice -> Int -> Bool -> String
readDataAssignmentsLowWorkerLineWC a index True =
    sliceName a ++ "_rd (" ++ show (index - lowAddress a) ++ ")"
readDataAssignmentsLowWorkerLineWC a index False =
    " or " ++ sliceName a ++ "_rd (" ++ show (index - lowAddress a) ++ ")"

addrWidth :: [Reg] -> Int
addrWidth a = log2 (highAddr a)

highAddr :: [Reg] -> Int
highAddr a = highAddrWorker a 0;

highAddrWorker :: [Reg] -> Int -> Int
highAddrWorker [] c = c
highAddrWorker [a] c
    | address a > c = address a
    | otherwise = c
highAddrWorker (a:b) c
    | address a > c = highAddrWorker b (address a)
    | otherwise = highAddrWorker b c

log2 :: Int -> Int
log2 a = log2Worker a 1

log2Worker :: Int -> Int -> Int
log2Worker a c
    | 2^c > a = c
    | otherwise = log2Worker a c+1

highIndex :: [Reg] -> Int
highIndex a = highIndexAr (makeRegList a)

highIndexAr :: [RegSlice] -> Int
highIndexAr a = highIndexArWorker a 0

highIndexArWorker :: [RegSlice] -> Int -> Int
highIndexArWorker [] c = c
highIndexArWorker [a] c = highIndexArWorkerC a c
highIndexArWorker (a:b) c = highIndexArWorker b (highIndexArWorkerC a c)

highIndexArWorkerC :: RegSlice -> Int -> Int
highIndexArWorkerC a b
    | highAddress a > b = highAddress a
    | otherwise = b

refineSources :: String -> String
refineSources a = intercalate "\n" (map refineSourcesLine (refineSourcesSplit a))

refineSourcesSplit :: String -> [String]
refineSourcesSplit a = splitOn "\n" a

refineSourcesLine :: String -> String
refineSourcesLine a
    | length a <= 80 = a
    | otherwise = splitLine a (getSpacesNumber a + 4)

getSpacesNumber :: String -> Int
getSpacesNumber a = getSpacesNumberWorker a 0

getSpacesNumberWorker :: String -> Int -> Int
getSpacesNumberWorker [] ret = ret
getSpacesNumberWorker [' '] ret = ret+1
getSpacesNumberWorker [a] ret = ret
getSpacesNumberWorker (' ':b) ret = getSpacesNumberWorker b (ret + 1)
getSpacesNumberWorker (a:b) ret = ret

splitLine :: String -> Int -> String
splitLine a offset = splitLineWorker a "" False offset

splitLineWorker :: String -> String -> Bool -> Int -> String
splitLineWorker a ret True offset = a ++ ret
splitLineWorker a ret _ offset = splitLineWorkerMux a ret offset

makeIndentation :: Int -> String
makeIndentation a = makeIndentationWorker a ""

makeIndentationWorker :: Int -> String -> String
makeIndentationWorker 0 ret = ret
makeIndentationWorker a ret = " " ++ makeIndentationWorker (a - 1) ret

splitLineWorkerMux :: String -> String -> Int -> String
splitLineWorkerMux [] ret offset = splitLineWorker [] ret True offset
splitLineWorkerMux [a] ret offset = splitLineWorker [] ([a] ++ ret) True offset
splitLineWorkerMux a ret offset = let
        (end, add) = splitLineWorkerAction (last a) (length a >= 80)
    in
        splitLineWorkerMux2 (init a) add ret end offset

splitLineWorkerMux2 :: String -> String -> String -> Bool -> Int -> String
splitLineWorkerMux2 a add ret c offset
    | c = splitLineWorker [] (a ++ add ++ (makeIndentation offset) ++ ret) True offset
    | otherwise = splitLineWorker a (add ++ ret) False offset

splitLineWorkerAction :: Char -> Bool -> (Bool, String)
splitLineWorkerAction a True = (False, [a])
splitLineWorkerAction a _
    | a == ' ' = (True, "\n")
    | otherwise = (False, [a])
