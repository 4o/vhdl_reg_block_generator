{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

import Data.Aeson
import Data.Text
import Control.Applicative
import Control.Monad
import qualified Data.ByteString.Lazy as B
import GHC.Generics
import HdlRegsGenerator
import System.Environment

instance FromJSON RegConfig
instance ToJSON RegConfig

instance FromJSON Reg
instance ToJSON Reg

instance FromJSON RegSlice
instance ToJSON RegSlice

main :: IO ()
main = do
    args <- getArgs
    d <- (eitherDecode <$> B.readFile (ensureGetHead args)) :: IO (Either String RegConfig)
    case d of
        Left err -> putStrLn err
        Right ps -> putStrLn (writeTop ps)

ensureGetHead :: [String] -> String
ensureGetHead [] = ""
ensureGetHead a = Prelude.head a